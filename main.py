from scrabble_score import score 

word = input("Please enter a word to score: ")

print("\"" + word + "\" is worth " + str(score(word)) + " points")